// lớp đối tượng
function sinhvien(
  manhap,
  tennhap,
  emailnhap,
  matKhaunhap,
  toannhap,
  lynhap,
  hoanhap
) {
  this.ma = manhap;
  this.ten = tennhap;
  this.email = emailnhap;
  this.matKhau = matKhaunhap;
  this.toan = toannhap;
  this.ly = lynhap;
  this.hoa = hoanhap;
  this.tinhdtb = function () {
    return (this.toan * 1 + this.ly * 1 + this.hoa * 1) / 3;
  };
}
