var dssv = [];

const DSSV_LOCAL = "DSSV_LOCAL";

// khi user load trang --> lấy dữ liệu từ localstorage
var laydulieulocal = localStorage.getItem("DSSV_LOCAL");

if (laydulieulocal != null) {
  // lấy dữ liệu cũ
  // //  laydulieulocal != 0 nếu tìm thấy thì
  // dssv = JSON.parse(laydulieulocal);
  // lấy dữ liệu mới ko mất function
  dssv = JSON.parse(laydulieulocal).map(function (item) {
    // khi lấy (localstorage ko còn function())
    // item : phần tử của array trong các lần lặp
    // return item.ten; -->tra về tên
    // return cua map để luu lại
    return new sinhvien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
  });

  // console.log(dssv);
  // truyền dữ liệu cho mảng dssv = convert lại kiểu string
  //   khi luu xuống là luudssvlocal= laydulieulocal
  renderdssv(dssv); //tạo giao diện
}

function themsv() {
  var thongtin = laythongtintuform();
  // add thông tin  của sv vào mảng
  dssv.push(thongtin);
  // convert sang kiểu string để lưu
  Addlocalstorage = JSON.stringify(dssv);
  // // lưu xuống localstorage
  localStorage.setItem("DSSV_LOCAL", Addlocalstorage);
  //    luudssv là tên lưu xuống
  // addlocalstorage là kiều dữ liệu đã convert
  // hiển thị lên màn hình
  renderdssv(dssv); //tạo giao diện
  console.log("dssv: ", dssv);
}
// khi load trang còn danh sách
// bạn đặt sai tên key nhé
// "DSSV_LOCAL" chứ k phải như luudssvlocal

// xóa sinh viên
function xoasv(id) {
  // tìm vị trí sinh viên cần xóa dựa vào id(id nay ko dc đổi)
  var vitri = dssv.findIndex(function (item) {
    return item.ma == id;
    // item là đối tượng sviên. mà có mã ~~ id
  });
  // xóa dùng hàm splice
  dssv.splice(vitri, 1);
  // dssv xóa tại phần tử (vị trí), 1 phần tử
  Addlocalstorage = JSON.stringify(dssv);
  // // lưu xuống localstorage
  localStorage.setItem("DSSV_LOCAL", Addlocalstorage);
  //    luudssv là tên lưu xuống
  // addlocalstorage là kiều dữ liệu đã convert
  // hiển thị lên màn hình
  renderdssv(dssv); //tạo giao diện
}
// sửa sinh viên
function suasv(id) {
  //  tìm vị trí sinh viên cần sửa
  var vitri = dssv.findIndex(function (item) {
    return item.ma == id;
    // item là đối tượng sviên. mà có mã ~~ id
  });
  //   truyền dữ liệu sinh viên cần sửa lên form
  var svsua = dssv[vitri];
  document.getElementById("txtMaSV").value = svsua.ma;
  document.getElementById("txtTenSV").value = svsua.ten;
  document.getElementById("txtEmail").value = svsua.email;
  document.getElementById("txtPass").value = svsua.matkhau;
  document.getElementById("txtDiemToan").value = svsua.toan;
  document.getElementById("txtDiemLy").value = svsua.ly;
  document.getElementById("txtDiemHoa").value = svsua.hoa;
}
// cập nhật sinh viên
function capnhat() {
  // lấy thông tin từ form truyền vào
  var svcapnhat = laythongtintuform();
  //do ko trực tiếp truyền id như hàm sửa và xóa,
  //   mà chỉ tìm sv có mã~~mã của laythongtintuform để cập nhật
  // tìm vị trí sinh viên cần cập nhật
  var vitri = dssv.findIndex(function (item) {
    return item.ma == svcapnhat.ma;
  });
  // push dữ liệu từ form vào dssv[vitri]
  dssv[vitri] = svcapnhat;
  Addlocalstorage = JSON.stringify(dssv);
  // // lưu xuống localstorage
  localStorage.setItem("DSSV_LOCAL", Addlocalstorage);
  //    luudssv là tên lưu xuống
  // addlocalstorage là kiều dữ liệu đã convert
  // hiển thị lên màn hình

  renderdssv(dssv); //tạo giao diện
}
// reset form
function reset() {
  // trỏ tới thẻ chứa các form (ma,ten,email... )và reset
  document.getElementById("formQLSV").reset();
}
// search sinh viên
function searchsv() {
  // tìm vị trí sinh viên có tên ~~ tên truyền vào
  var tensvsearch = document.getElementById("txtSearch").value;
  var vitri = dssv.findIndex(function (item) {
    return item.ten == tensvsearch;
  });
  var svsearch = dssv[vitri];
  console.log("svsearch: ", svsearch);
  //   show thông tin sinh viên lên form
  var svsua = dssv[vitri];
  document.getElementById("txtMaSV").value = svsua.ma;
  document.getElementById("txtTenSV").value = svsua.ten;
  document.getElementById("txtEmail").value = svsua.email;
  document.getElementById("txtPass").value = svsua.matkhau;
  document.getElementById("txtDiemToan").value = svsua.toan;
  document.getElementById("txtDiemLy").value = svsua.ly;
  document.getElementById("txtDiemHoa").value = svsua.hoa;
  // end show thông tin lên form
  //loc dssv theo tên để cập nhật
  var filterSV = dssv.filter(function (item) {
    return item.ten == tensvsearch;
  });
  if (filterSV.length > 0) {
    renderdssv(filterSV);
  }
}
